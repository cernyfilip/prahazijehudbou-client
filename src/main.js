import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import cors from 'cors'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import Default from './layouts/Default.vue'
import AdminLayout from './layouts/NoSideBar.vue'
import Empty from './layouts/Empty.vue'

Vue.use(Buefy)
Vue.use(cors)
Vue.component('default-layout', Default)
Vue.component('no-sidebar-layout', AdminLayout)
Vue.component('empty-layout', Empty)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
