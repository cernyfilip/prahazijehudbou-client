import Vue from 'vue'
import Router from 'vue-router'
import store from './store';
import userForm from './components/userForm'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: { layout: 'no-sidebar' },
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/admin/users',
      name: 'users',
      component: () => import(/* webpackChunkName: "about" */ './views/Users.vue')
    },
    {
      path: '/admin/stages',
      name: 'stages',
      component: () => import(/* webpackChunkName: "about" */ './views/Stages.vue')
    },
    {
      path: '/admin/performances',
      name: 'performances',
      component: () => import(/* webpackChunkName: "about" */ './views/Performances.vue')
    },
    {
      path: '/admin/volunteers',
      name: 'volunteers',
      component: () => import(/* webpackChunkName: "about" */ './views/Volunteers.vue')
    },
    {
      path: '/users-signup',
      name: 'usersSignup',
      component: () => import(/* webpackChunkName: "about" */ './components/userForm.vue')
    },
    {
      path: '*',
      redirect: '/login'
    }
  ]
})

router.beforeEach((to, from, next) => {
  store.dispatch('fetchAccessToken');
  if (to.fullPath === '/admin/*') {
    if (!store.state.accessToken) {
      next('/login');
    }
  }
  if (to.fullPath === '/login') {
    if (store.state.accessToken) {
      next('/users');
    }
  }
  next();
});

export default router;
