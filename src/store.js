import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    accessToken: null,
    loggingIn: false,
    loginError: null,
    performers: [],
    performer: [],
    volunteers: [],
    volunteer: []
  },
  mutations: {
    loginStart: state => state.loggingIn = true,
    loginStop: (state, errorMessage) => {
      state.loggingIn = false
      state.loginError = errorMessage
    },
    updateAccessToken: (state, accessToken) => {
      state.accessToken = accessToken
    },
    logout: (state) => {
      state.accessToken = null
    },
    updatePerformers (state, performers) {
      state.performers = performers
    },
    updatePerformer (state, performer) {
      state.performer = performer
    },
    updateVolunteers (state, volunteers) {
      state.volunteers = volunteers
    },
    updateVolunteer (state, volunteer) {
      state.volunteer = volunteer
    }
  },
  actions: {
    doLogin ({ commit }, loginData) {
      commit('loginStart')

      axios.post('https://api.nerudnyfest.cz/v1/user/login', {
        ...loginData
      })
        .then(response => {
          localStorage.setItem('accessToken', response.data.token)
          commit('loginStop', null)
          commit('updateAccessToken', response.data.token)
        })
        .then(() => {
          router.push('/admin/users')
        })
        .catch(error => {
          commit('loginStop', error.response.data.error)
          commit('updateAccessToken', null)
        })
    },
    fetchAccessToken ({ commit }) {
      commit('updateAccessToken', localStorage.getItem('accessToken'))
    },
    logout ({ commit }) {
      localStorage.removeItem('accessToken')
      commit('logout')
      router.push('/login')
    },
    getVolunteers({ commit }) {
      return new Promise(( resolve, reject)=> {
        axios.get('https://api.nerudnyfest.cz/v1/volunteer')
        .then((response) => {
          commit('updateVolunteers', response.data)
          resolve()
        })
        .catch( error => {
          console.log(error)
          
        })
      })

    },
    getVolunteer ({ commit }, id) {
      axios.get('https://api.nerudnyfest.cz/v1/volunteer/' + id)
        .then((response) => {
          commit('updateVolunteer', response.data)
        })
        .catch(error => {
          console.log(error.response)
        })
    },
    getPerformers ({ commit }) {
      return new Promise (( resolve, reject) => {
        axios.get('https://api.nerudnyfest.cz/v1/performer')
        .then((response) => {
          commit('updatePerformers', response.data)
          resolve()
        })
        .catch(error => {
          console.log(error.response)
        })
      })  
    },
    getPerformer ({ commit }, id) {
      axios.get('https://api.nerudnyfest.cz/v1/performer/' + id)
        .then((response) => {
          commit('updatePerformer', response.data)
        })
        .catch(error => {
          console.log(error.response)
        })
    },
    updatePerformer ({ commit }, id, payload) {
      axios.put('https://api.nerudnyfest.cz/v1/performer/' + id, {
        payload
      })
        .then(response => {
          console.log(response)
        })
        .catch(error => {
          console.log(error)
        })
    }
  }
})
